<?php

namespace WPez\WPezTraits\ArrayAssocInsert;

trait TraitArrayAssocInsert {

    protected function arrayAssocInsert( $arr_input, $arr_insert, $int_col = false ) {

        if ( $int_col === false ) {
            $int_col = count( $arr_input );
        }

        return array_slice( (array)$arr_input, 0, $int_col, true ) +
               (array)$arr_insert +
               array_slice( (array)$arr_input, $int_col, null, true );

    }
}