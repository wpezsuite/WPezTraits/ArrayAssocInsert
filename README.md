## WPezTraits: Array Assoc Insert

__Insert into an associative array at a specified column index.__

As inspired by: https://stackoverflow.com/questions/3353745/how-to-insert-element-into-arrays-at-specific-position


> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --